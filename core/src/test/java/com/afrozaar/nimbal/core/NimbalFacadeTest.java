package com.afrozaar.nimbal.core;

import static org.assertj.core.api.Assertions.assertThat;

import com.afrozaar.nimbal.api.ErrorLoadingArtifactException;
import com.afrozaar.nimbal.api.INimbalService;
import com.afrozaar.nimbal.api.ModuleLoadException;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;

public class NimbalFacadeTest {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(NimbalFacadeTest.class);

    private INimbalService facade;

    private AnnotationConfigApplicationContext testApplicationContext;

    private MavenRepositoriesManager setupDefaultMavenRepo() {
        MavenRepositoriesManager manager = new MavenRepositoriesManager(null, false);
        manager.setM2Folder(".m2");
        manager.init();
        return manager;
    }

    @Before
    public void setupTestAppContext() {
        this.testApplicationContext = new AnnotationConfigApplicationContext(TestConfiguration.class);
        MavenRepositoriesManager manager = setupDefaultMavenRepo();
        facade = new NimbalService(new ContextLoader.Builder().setMavenRepositoriesManager(setupDefaultMavenRepo()).build());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void SimpleFacade() throws MalformedURLException, ClassNotFoundException, ErrorLoadingArtifactException, ModuleLoadException, IOException {
        Module module = facade.loadModule(new MavenCoords("com.afrozaar.nimbal.test", "nimbal-test-module-annotation", "1.0.0-SNAPSHOT"));

        assertThat(facade.getModule(module.getName())).isNotNull();
    }
}
