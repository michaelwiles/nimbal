package com.afrozaar.nimbal.core;

import com.afrozaar.nimbal.api.ErrorLoadingArtifactException;
import com.afrozaar.nimbal.api.Event;
import com.afrozaar.nimbal.api.INimbalRepository;
import com.afrozaar.nimbal.api.INimbalService;
import com.afrozaar.nimbal.api.Listener;
import com.afrozaar.nimbal.api.ModuleLoadException;

import org.springframework.cglib.proxy.Proxy;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.function.Consumer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class NimbalService implements INimbalService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(NimbalService.class);

    private ContextLoader contextLoader;

    private INimbalRepository repository = (INimbalRepository) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[] { INimbalRepository.class },
            (proxy, method, args) -> {
                LOG.debug("called {} with {}", method.getName(), args);
                return null;
            });

    private SimpleRegistry registry;

    private Listener listener = new Listener() {

        @Override
        public void event(MavenCoords coords, Event status) {
            repository.set(coords, status);
        }

        @Override
        public void event(String moduleName, Event status) {
            repository.set(moduleName, status);
        }

        @Override
        public void event(MavenCoords coords, Event status, Exception e) {
            repository.set(coords, status, e.getMessage());

        }

    };

    public NimbalService(ContextLoader contextLoader) {
        super();
        this.contextLoader = contextLoader;
        contextLoader.setListener(listener);
        this.registry = contextLoader.getRegistry();
    }

    @Override
    public Module loadModule(MavenCoords mavenCoords, Consumer<ConfigurableApplicationContext>... preRefresh) throws ErrorLoadingArtifactException,
            ModuleLoadException, MalformedURLException, IOException, ClassNotFoundException {
        try {
            return contextLoader.loadModule(mavenCoords, preRefresh);
        } catch (Exception e) {
            LOG.error("error loading module", e);
            listener.event(mavenCoords, Event.ERROR, e);
            throw e;
        }
    }

    @Override
    public void unloadModule(String moduleName) throws ModuleLoadException {
        contextLoader.unloadModule(moduleName);
    }

    @Override
    public SimpleRegistry getRegistry() {
        return registry;
    }

    @Override
    public Module getModule(String name) {
        return registry.getModule(name);
    }

    @Override
    public Map<String, Module> getModules() {
        return registry.getModules();
    }

    public void close() {
        Set<Module> modules = new LinkedHashSet<Module>();
        registry.getModules().values().forEach(m -> add(modules, m));
    }

    /**
     * does a kind of an 
     * @param modules
     * @param module
     */
    private void add(Set<Module> modules, Module module) {
        module.getChildren().forEach(x -> add(modules, module));
        modules.add(module);
    }
}
