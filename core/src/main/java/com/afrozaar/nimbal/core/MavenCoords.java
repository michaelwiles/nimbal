/*******************************************************************************
 * Nimbal Module Manager 
 * Copyright (c) 2017 Afrozaar.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Apache License 2.0
 * which accompanies this distribution and is available at https://www.apache.org/licenses/LICENSE-2.0
 *******************************************************************************/
package com.afrozaar.nimbal.core;

import static java.lang.String.format;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

public class MavenCoords {
    @JsonAutoDetect(fieldVisibility = Visibility.ANY)
    public static class ModuleKey {

        private String group;
        private String artifactId;

        public ModuleKey(MavenCoords mavenCoords) {
            this.group = mavenCoords.getGroupId();
            this.artifactId = mavenCoords.getArtifactId();
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((artifactId == null) ? 0 : artifactId.hashCode());
            result = prime * result + ((group == null) ? 0 : group.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            ModuleKey other = (ModuleKey) obj;
            if (artifactId == null) {
                if (other.artifactId != null) {
                    return false;
                }
            } else if (!artifactId.equals(other.artifactId)) {
                return false;
            }
            if (group == null) {
                if (other.group != null) {
                    return false;
                }
            } else if (!group.equals(other.group)) {
                return false;
            }
            return true;
        }

    }

    private String groupId;
    private String artifactId;
    private String version;
    
    private String moduleName;

    public MavenCoords(String groupId, String artifactId, String version) {
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.version = version;
    }

    public String getAsString() {
        //com.afrozaar.ashes.module:ashes-sbp-scheduled:jar:module:0.4.0-SNAPSHOT
        return format("%s:%s:%s", groupId, artifactId, version);
    }

    @Override
    public String toString() {
        return "MavenCoords [groupId=" + groupId + ", artifactId=" + artifactId + ", version=" + version + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((artifactId == null) ? 0 : artifactId.hashCode());
        result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MavenCoords other = (MavenCoords) obj;
        if (artifactId == null) {
            if (other.artifactId != null) {
                return false;
            }
        } else if (!artifactId.equals(other.artifactId)) {
            return false;
        }
        if (groupId == null) {
            if (other.groupId != null) {
                return false;
            }
        } else if (!groupId.equals(other.groupId)) {
            return false;
        }
        if (version == null) {
            if (other.version != null) {
                return false;
            }
        } else if (!version.equals(other.version)) {
            return false;
        }
        return true;
    }

    public boolean isStable() {
        return !isSnapshot();
    }

    public boolean isSnapshot() {
        return version != null && version.endsWith("SNAPSHOT");
    }

    public String getGroupId() {
        return groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public String getVersion() {
        return version;
    }

    public ModuleKey getModuleKey() {
        return new ModuleKey(this);
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }
}