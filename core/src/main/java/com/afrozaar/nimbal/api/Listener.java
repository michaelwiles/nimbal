package com.afrozaar.nimbal.api;

import com.afrozaar.nimbal.core.MavenCoords;

public interface Listener {

    void event(MavenCoords coords, Event status);

    void event(MavenCoords coords, Event status, Exception e);

    void event(String moduleName, Event status);

}
