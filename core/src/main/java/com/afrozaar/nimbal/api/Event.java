package com.afrozaar.nimbal.api;

public enum Event {

    LOADING, ERROR, REFRESHING, UNLOADED, LOADED;
}
