package com.afrozaar.nimbal.annotations;

public interface IModuleConfigurer {

    default void beforeRefresh() {}

    default void afterRefresh() {}

}
